require("dotenv").config();
const [ express, cors, morgan, defaultRouter ] = [
    require("express"),
    require("cors"),
    require("morgan"),
    require("./route/index.js"),
];

const app = express();
const logger = require('./config/winston');
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(morgan("combined"));


app.use("/", defaultRouter);
app.use("/*", ( req, res) => {
   res.status(400).json({
       message: "no content"
   })
});

app.listen(PORT, () => {
    console.log(`SERVER RUNNING ===> http://localhost:${PORT}`);
});
