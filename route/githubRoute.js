const express = require('express');
const Router = express.Router();
const { GithubService } = require("../controller");

Router.get("/getuser/:username", GithubService.GetUser );
Router.get("/getallrepo/:username",GithubService.GetAllRepos);
Router.get("/getrepo/:username/:repo", GithubService.GetRepoUser);

Router.use((err, req, res, next) => {
  if (err) {
    res.status(500).json({ error: err.message });
  }
});

module.exports = Router;
