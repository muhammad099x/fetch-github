const express = require("express");
const Router = express.Router();

const [ githubRoute ] = [ require("./githubRoute.js")];

Router.use("/api", githubRoute);

module.exports = Router;
