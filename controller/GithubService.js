const GIT = require("../API.js");
const winston = require("../config/winston");

class GithubService {
    static async GetUser(req, res, next) {
        const { username } = req.params;
        try {
            const { data } = await GIT.get(`/users/${username}`);
            res.status(200).json(data);
        } catch (error) {
            if( error.response.status >= 400 ) {
                winston.log({
                    level: "error",
                    message: error.response.data.message,
                });
                res.status(404).json({ error: error.response.data.message});
            }
            next(error);
        }
    }

    static async GetRepoUser(req, res, next) {
        const { username, repo } = req.params;
        try {
            const { data } = await GIT.get(`/repos/${username}/${repo}/readme`, {
                headers: {
                    "Accept": "application/vnd.github.html",
                }
            });
            res.status(200).json(data);
        } catch (error) {
            if( error.response.status >= 400 ) {
                winston.log({
                    level: "error",
                    message: error.response.data.message,
                });
                res.status(404).json({ error: error.response.data.message});
            }
            next(error);
        }
    }

    static async GetAllRepos(req, res, next) {
        const { username } = req.params;
        try {
            const { data } = await GIT.get(`/users/${username}/repos`);
            if( data.length === 0 ) {
                winston.log({
                    level: "error",
                    message: 'repo is empty'
                });
                res.status(200).json({ message: 'repo is empty' })
            } else {
                res.status(200).json(data);
            }
        } catch (error) {
            if( error.response.status >= 400 ) {
                winston.log({
                    level: "error",
                    message: error.response.data.message,
                });
                res.status(404).json({ error: error.response.data.message});
            }
            next(error);
        }
    }
}

module.exports = GithubService;
