const appRoot = require('app-root-path');
// const winston = require('winston');


const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint } = format;
// define the custom settings for each transport (file, console)

const options = {
    file: {
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

const logger = createLogger({
    format: combine(
        label({ label: 'github' }),
        timestamp(),
        prettyPrint()
    ),
    transports: [
        new transports.File(options.file),
        new transports.Console(options.console)
    ]
});

// logger.log({
//     level: 'info',
//     message: 'What time is the testing at?'
// });
// Outputs:
// { level: 'info',
//   message: 'What time is the testing at?',
//   label: 'right meow!',
//   timestamp: '2017-09-30T03:57:26.875Z' }


// instantiate a new Winston Logger with the settings defined above
// const logger = winston.createLogger({
//     transports: [
//         new winston.transports.File(options.file),
//         new winston.transports.Console(options.console)
//     ],
//     exitOnError: false, // do not exit on handled exceptions
// });
//
// // create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
    write: function(message) {
        // console.log(" iniii message  ===> ", message )
        // use the 'info' log level so the output will be picked up by both transports (file and console)
        logger.info(message);
    },
};

module.exports = logger;

